<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Meu portfólio pessoal e profissional de projetos web!">
    <link rel="shortcut icon" href="./assets/images/post-stamp.ico" type="image/x-icon">

    <link rel="stylesheet" href="assets/css/main.css">

    <title>Thomas Almeida - Portfólio</title>
</head>

<body>

    <?php
    //Navbar included
    include 'assets/includes/navbar.php';
    ?>

    <div class="container">

        <div class="container-content">

            <header class="user-post-area">

                <section class="user-post-text">
                    <section class="post">
                        <section class="user-post-section">
                            <img src="assets/images/profile-pic-koala.png" id="pic-box" class="picture-box  profile-img-post" id="pic-intro" alt="">
                            <section>
                                <h1 id="username">Thomas Almeida</h1>
                                <section class="username-link">
                                    <a href="https://www.instagram.com/eu.thommas/" target="_blank" class="arroba">&#128247@eu.thomas</a>
                                    <img src="assets/images/verficado.png" alt="">
                                </section>
                            </section>
                        </section>

                        <h3 class="time-posted">Bem-vindo ao meu portfólio &#128122</h3>

                        <p class="post-description">
                            Um desenvolvedor que adora um side project kkk, criar projetos que me permitem aprender mais sobre uma tecnologia ou stack, editar e criar novas funcionalidades e me divertir nisso são coisas que me deixam muito mais motivado, os projetos abaixo são uma base dos meus conhecimentos e práticas colocadas em projetos pessoais, você também pode acessar os repositórios para clonar e saber mais, espero que se divirta navegando!
                        </p>

                        <section class="github-section">
                            <b>Atualmente Dev Front End no <a href="https://www.instagram.com/grupoidealtrends/" class="arroba" target="_blank">@grupoidealtrends</a></b>
                        </section>

                    </section>

                </section>

            </header>

            <section class="post-feed">

                <section id="post-area">

                    <section class="post">

                        <section class="user-post-section">
                            <img src="assets/images/profile-pic-koala.png" class="picture-box  profile-img-post" alt="">
                            <h3 class="post-username">Thomas Almeida</b></h3>
                        </section>

                        <p class="time-posted">postado em <strong>17/07/2021 ás 18h</strong></p>

                        <h2 class="title-project">Pomodor Music Clock <b>novo</b></h2>

                        <p class="post-description">
                            Tentando aprender como controlar segundos e minutos com <strong class="javascript">javascript</strong>, uma maneira bacana de usar isso seria fazendo um relógio pomodoro, onde eu poderia além de aprender como se faz, usar no meu dia-a-dia, e claro com um toque a mais que é a música, junto disso também está implementado um tocador com algumas músicas e ficou bem massa!!
                        </p>


                        <section class="github-section">
                            <h3>Acessar repositório no <a href="https://github.com/ThomAlmeidaRD/Pomodoro-Music-Clock" target="_blank">Github </a>✔️</h3>
                        </section>

                        <section class="post-image">

                            <div class="slideshow-container">

                                <div class="mySlides fade">
                                    <div class="numbertext">1 / 3</div>
                                    <img src="assets/images/pomodoro-music-2.png" draggable="false" style="width:100%">
                                    <div class="text">O relógio fica disponível junto ao player...</div>
                                </div>

                                <div class="mySlides fade">
                                    <div class="numbertext">2 / 3</div>
                                    <img src="assets/images/pomodoro-music-3.png" draggable="false" style="width:100%">
                                    <div class="text">Enquanto o relógio anda, você pode ouvir seus sons...</div>
                                </div>

                                <div class="mySlides fade">
                                    <div class="numbertext">3 / 3</div>
                                    <img src="assets/images/pomodoro-music-1.png" draggable="false" style="width:100%">
                                    <div class="text">Ao clicar no nome do artista, você abre ele no Spotify</div>
                                </div>

                                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                <a class="next" onclick="plusSlides(1)">&#10095;</a>

                            </div>
                            <br>

                            <div style="text-align:center">
                                <span class="dot" onclick="currentSlide(1)"></span>
                                <span class="dot" onclick="currentSlide(2)"></span>
                                <span class="dot" onclick="currentSlide(3)"></span>
                            </div>

                        </section>


                        <section class="actions">
                            <section>
                                <h3>Tecnologias usadas</h3>
                            </section>
                            <a href="https://developer.mozilla.org/pt-BR/docs/Web/HTML" target="_blank" class="remove-btn" id="html"><img src="">HTML</a>
                            <a href="https://developer.mozilla.org/pt-BR/docs/Web/css" target="_blank" class="remove-btn" id="css"><img src="">CSS</a>
                            <a href="https://developer.mozilla.org/pt-BR/docs/Web/javascript" target="_blank" class="remove-btn" id="jscript"><img src="">JS</a>
                        </section>


                    </section>

                    <section class="post" id="old-post-1">

                        <section class="user-post-section">
                            <img src="assets/images/profile-pic-koala.png" class="picture-box  profile-img-post" alt="">
                            <h3 class="post-username">Thomas Almeida</h3>
                        </section>

                        <p class="time-posted">postado em <strong>17/07/2021 ás 18h</strong></p>

                        <h2 class="title-project">Título do projeto </h2>

                        <p class="post-description">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perferendis totam
                            quod eius cupiditate obcaecati ipsum.Lorem, ipsum dolor sit amet consectetur
                            adipisicing elit. Perferendis totam quod eius cupiditate obcaecati ipsum.
                        </p>


                        <section class="github-section">
                            <h3>Acessar repositório no <a href="#">Github </a>✔️</h3>
                        </section>

                        <section class="post-image">

                            <div class="slideshow-container">

                                <div class="mySlides2 fade">
                                    <div class="numbertext">1 / 3</div>
                                    <img src="assets/images/post-example.png" draggable="false" style="width:100%">
                                    <div class="text">Caption Text</div>
                                </div>

                                <div class="mySlides2 fade">
                                    <div class="numbertext">2 / 3</div>
                                    <img src="assets/images/model-2.jpeg" draggable="false" style="width:100%">
                                    <div class="text">Caption Two</div>
                                </div>

                                <div class="mySlides2 fade">
                                    <div class="numbertext">3 / 3</div>
                                    <img src="assets/images/model-3.webp" draggable="false" style="width:100%">
                                    <div class="text">Caption Three</div>
                                </div>

                                <a class="prev" onclick="plusSlides2(-1)">&#10094;</a>
                                <a class="next" onclick="plusSlides2(1)">&#10095;</a>

                            </div>
                            <br>

                            <div style="text-align:center">
                                <span class="dot" onclick="currentSlide2(1)"></span>
                                <span class="dot" onclick="currentSlide2(2)"></span>
                                <span class="dot" onclick="currentSlide2(3)"></span>
                            </div>


                        </section>

                        <section class="actions">
                            <section>
                                <h3>Tecnologias usadas</h3>
                            </section>
                            <a href="https://developer.mozilla.org/pt-BR/docs/Web/HTML" target="_blank" class="remove-btn" id="html"><img src="">HTML</a>
                            <a href="https://developer.mozilla.org/pt-BR/docs/Web/css" target="_blank" class="remove-btn" id="css"><img src="">CSS</a>
                            <a href="https://developer.mozilla.org/pt-BR/docs/Web/javascript" target="_blank" class="remove-btn" id="jscript"><img src="">JS</a>
                        </section>
                    </section>

                </section>

            </section>

        </div>

    </div>

</body>

<script src="assets/scripts/make-post.js"></script>
<script src="assets/scripts/slider.js"></script>
<script src="assets/scripts/news-filter.js"></script>

</html>